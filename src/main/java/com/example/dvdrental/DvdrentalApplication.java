package com.example.dvdrental;

import com.example.dvdrental.model.Actor;
import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MappedTypes(Actor.class)
@MapperScan("com.example.dvdrental.mapper")
@SpringBootApplication
public class DvdrentalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvdrentalApplication.class, args);
	}

}
