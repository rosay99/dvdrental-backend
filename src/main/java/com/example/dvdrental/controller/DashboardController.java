package com.example.dvdrental.controller;

import com.example.dvdrental.model.Actor;
import com.example.dvdrental.service.ActorServiceImpl;
import com.example.dvdrental.service.FilmActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/dashboard")
@CrossOrigin(origins = "http://localhost:3000")
public class DashboardController {
    @Autowired
    private ActorServiceImpl actorService;
    @Autowired
    private FilmActorService filmActorService;

    @GetMapping("/actor/all")
    private ResponseEntity<?> getAllActor(){
        return actorService.getAll();
    }
    @PostMapping("/actor/add")
    private ResponseEntity<?> addNewActor(@RequestBody Actor actor){
        return actorService.addNewActor(actor);
    }
    @PutMapping("/actor/update")
    private ResponseEntity<?> updateActor(@RequestBody Actor actor){
        return actorService.updateActor(actor);
    }
    @DeleteMapping("/actor/delete/{id}")
    private ResponseEntity<?> deleteActor(@PathVariable Integer id){
        return actorService.deleteActor(id);
    }
    @GetMapping("/film/{id}")
    private ResponseEntity<?> getFilm(@PathVariable Integer id){
        return filmActorService.getByActor(id);
    }
}
