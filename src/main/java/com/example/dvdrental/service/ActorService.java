package com.example.dvdrental.service;

import com.example.dvdrental.model.Actor;
import org.springframework.http.ResponseEntity;
public interface ActorService {
    public ResponseEntity<?> getAll();
    public ResponseEntity<?> addNewActor(Actor actor);
}
