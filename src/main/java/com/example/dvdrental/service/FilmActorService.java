package com.example.dvdrental.service;

import com.example.dvdrental.mapper.FilmActorMapper;
import com.example.dvdrental.model.FilmActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FilmActorService {
    @Autowired
    private FilmActorMapper filmActorMapper;

    public ResponseEntity<?> getByActor(Integer id){
        try{
            List<FilmActor> fa = filmActorMapper.findByActor(id);
            return ResponseEntity.ok().body(fa);
        }catch(Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
