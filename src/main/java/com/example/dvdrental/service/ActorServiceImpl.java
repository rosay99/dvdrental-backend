package com.example.dvdrental.service;

import com.example.dvdrental.model.Actor;
import com.example.dvdrental.mapper.ActorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class ActorServiceImpl implements ActorService{
    @Autowired
    private ActorMapper actorMapper;
    @Override
    public ResponseEntity<?> getAll() {
        try{
            List<Actor> res = actorMapper.findAll();
            return ResponseEntity.ok().body(res);
        } catch(Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> addNewActor(Actor actor) {
        try{
            LocalDateTime dateTime = LocalDateTime.now();
            Actor act = new Actor();
            act.setFirst_name(actor.getFirst_name());
            act.setLast_name(actor.getLast_name());
            act.setLast_update(Timestamp.valueOf(dateTime));
            actorMapper.insert(act);
            return ResponseEntity.ok().body("Insert successful");
        }catch(Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity<?> updateActor(Actor actor){
        try{
            LocalDateTime dateTime = LocalDateTime.now();
            Actor act = new Actor();
            act.setLast_name(actor.getLast_name());
            act.setFirst_name(actor.getFirst_name());
            act.setLast_update(Timestamp.valueOf(dateTime));
            act.setActor_id(actor.getActor_id());
            actorMapper.update(act);
            return ResponseEntity.ok().body(act);
        }catch(Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity<?> deleteActor(Integer id){
        try{
            actorMapper.delete(id);
            return ResponseEntity.ok().body("Actor's data deleted");
        }catch(Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
