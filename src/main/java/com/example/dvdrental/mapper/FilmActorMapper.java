package com.example.dvdrental.mapper;

import com.example.dvdrental.model.FilmActor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FilmActorMapper {
    @Select("select film_actor.film_id,film.title,film.description,film.release_year,language.name from film_actor join film on film_actor.film_id = film.film_id join language on film.language_id = language.language_id where actor_id=#{actor_id}")
    public List<FilmActor> findByActor(Integer actor_id);
}
