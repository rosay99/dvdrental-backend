package com.example.dvdrental.mapper;

import com.example.dvdrental.model.Actor;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ActorMapper {
    @Select("select * from actor order by actor_id desc limit 100")
    public List<Actor> findAll();

    @Insert("insert into actor(first_name, last_name, last_update) " +
            " values (#{first_name}, #{last_name}, #{last_update})")
    public int insert(Actor actor);

    @Update("update actor set first_name=#{first_name}, " +
            " last_name=#{last_name}, last_update=#{last_update} where actor_id=#{actor_id}")
    public int update(Actor actor);

    @Delete("delete from actor where actor_id = #{actor_id}")
    public int delete(Integer id);
}
