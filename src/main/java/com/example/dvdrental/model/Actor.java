package com.example.dvdrental.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Actor {
    private Integer actor_id;
    private String first_name;
    private String last_name;
    private Timestamp last_update;
}
