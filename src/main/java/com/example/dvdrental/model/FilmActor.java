package com.example.dvdrental.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Year;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmActor {
    private Integer film_id;
    private String title;
    private String description;
    private Year release_year;
    private String name;
}
